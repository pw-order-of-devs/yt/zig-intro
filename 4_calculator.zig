const std = @import("std");

const CalcError = error {
    ParseInputError,
    UnsupportedOperationError,
};

const RPN = struct {
    num_1: f64,
    num_2: f64,
    op: []const u8,

    fn calculate(self: *RPN) !f64 {
        if (std.mem.eql(u8, self.op, "+")) {
            return self.num_1 + self.num_2;
        } else if (std.mem.eql(u8, self.op, "-")) {
            return self.num_1 - self.num_2;
        } else if (std.mem.eql(u8, self.op, "*")) {
            return self.num_1 * self.num_2;
        } else if (std.mem.eql(u8, self.op, "/")) {
            return self.num_1 / self.num_2;
        } else {
            return CalcError.UnsupportedOperationError;
        }
    }

    fn print_result(self: *RPN, result: f64) void {
        std.debug.print("{} {s} {} = {}\n", .{self.num_1, self.op, self.num_2, result});
    }
};

fn print_help() void {
    std.debug.print("Zig calculator working on Reverse Polish Notation.\n", .{});
    std.debug.print("Example input:\n", .{});
    std.debug.print(">>> 3 4 +\n", .{});
    std.debug.print(">>> 7 2 -\n\n", .{});
    std.debug.print(">>> help - prints this menu\n", .{});
    std.debug.print(">>> exit - terminates the program\n\n", .{});
}

fn try_parse_input(buf: []u8) !RPN {
    var split = std.mem.split(u8, buf, " ");
    const allocator = std.heap.page_allocator;
    var list = std.ArrayList([]const u8).init(allocator);
    var item: ?[]const u8 = split.first();
    while (item != null) {
        if (item) |unwrapped| {
            try list.append(unwrapped);
        }
        item = split.next();
    }
    if (list.items.len != 3) return CalcError.ParseInputError;
    return RPN{
      .num_1 = try std.fmt.parseFloat(f64, list.items[0]),
      .num_2 = try std.fmt.parseFloat(f64, list.items[1]),
      .op = list.items[2],
    };
}

pub fn main() !void {
    const stdin = std.io.getStdIn().reader();

    while (true) {
    var buf: [128]u8 = undefined;
        std.debug.print(">>> ", .{ });
        if (try stdin.readUntilDelimiterOrEof(buf[0..], '\n')) |user_input| {
            if (std.mem.eql(u8, user_input, "exit")) break;
            if (std.mem.eql(u8, user_input, "help")) {
                print_help();
                continue;
            }

            var rpn = try_parse_input(user_input) catch {
                std.debug.print("failed parsing user input. invalid format\n", .{});
                continue;
            };

            if (rpn.calculate()) |result| {
                rpn.print_result(result);
            } else |err| {
                std.debug.print("Error: {}.\n", .{err});
            }
        } else {
            std.debug.print("failed parsing user input. try again\n", .{});
        }
    }
}
