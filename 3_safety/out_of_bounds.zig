test "memory safety - out of bounds" {
    var buffer: [4]u8 = undefined;
    buffer[5] = 42; // Compile-time error: Index out of bounds
}
