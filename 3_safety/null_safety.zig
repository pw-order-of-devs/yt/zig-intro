const std = @import("std");

pub fn printer(value: ?u8) void {
    if (value == null) {
        std.debug.print("No value set\n", .{});
    } else {
        std.debug.print("Value is {?}\n", .{value});
    }
}

pub fn main() !void {
    printer(null);
    printer(8);
    printer(255);
}
