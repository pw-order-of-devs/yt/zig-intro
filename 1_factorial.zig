const std = @import("std");

pub fn factorial(n: u64) u64 {
  if (n == 0) {
    return 1;
  } else {
    return n * factorial(n - 1);
  }
}

pub fn main() !void {
  var n: u64 = 5;
  var factorial_result: u64 = factorial(n);
  std.debug.print("The factorial of {} is {}.", .{n, factorial_result});
}
