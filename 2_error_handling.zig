const std = @import("std");

const Error = error {
  IOError,
  ParseError,
  OtherError,
};

pub fn some_function() !u8 {
  if (false) {
    // won't happen, hopefully ..
    return 1;
  } else {
    return Error.OtherError;
  }
}

test "error handling - case 0" {
  var res = some_function() catch 0;
  std.debug.print("Returned value is {}.\n\n", .{res});
}

test "error handling - case 1" {
  if (some_function()) |res| {
    std.debug.print("Returned value is {}.\n", .{res});
  } else |err| {
    std.debug.print("Error happened! {}.\n", .{err});
  }
}

test "error handling - case 2" {
  var res = try some_function();
  std.debug.print("It works! {}", .{res});
}
